package framework;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class WelcomeController {
    @GetMapping( "/" )
    public ModelAndView index ( @ModelAttribute ModelAndView mv ) {
        mv.setViewName( "index" );
        return mv;
    }
}
