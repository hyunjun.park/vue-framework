define( [
    'text!component/m001/m001.html',
    'text!component/m001/m001-search.html',
    'text!component/m001/m001-content.html'
], function ( m001_html, m001_search_html, m001_content_html ) {
'use strict';
return {
    components: {
        'search-component': SearchComponent,
        'content-component': ContentComponent
    },
    data: function () {
        return {
            msg: '메세지 영역'
        };
    },
    mounted(){
    },
    methods: {
        testClick() {
            alert('함수 확인')
            this.$emit('test','데이터 확인용 m001')
        }
    },
    template: m001_html
};

function SearchComponent ( resolve, reject ) {
    resolve( {
        template: m001_search_html
    } );
}

function ContentComponent ( resolve, reject ) {
    resolve( {
        template: m001_content_html
    } );
}

} );
