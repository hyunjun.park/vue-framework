define( [
    'text!component/m002/m002.html',
    'text!component/m002/m002-search.html',
    'text!component/m002/m002-content.html'
], function ( m002_html, m002_search_html, m002_content_html ) {
'use strict';
return {
    components: {
        'search-component': SearchComponent,
        'content-component': ContentComponent
    },
    template: m002_html
};

function SearchComponent ( resolve, reject ) {
    resolve( {
        template: m002_search_html
    } );
}

function ContentComponent ( resolve, reject ) {
    resolve( {
        template: m002_content_html
    } );
}

} );
