define( [
    'text!component/m003/m003.html',
    'text!component/m003/m003-search.html',
    'text!component/m003/m003-content.html'
], function ( m003_html, m003_search_html, m003_content_html ) {
'use strict';
return {
    components: {
        'search-component': SearchComponent,
        'content-component': ContentComponent
    },
    template: m003_html
};

function SearchComponent ( resolve, reject ) {
    resolve( {
        template: m003_search_html
    } );
}

function ContentComponent ( resolve, reject ) {
    resolve( {
        template: m003_content_html
    } );
}

} );
