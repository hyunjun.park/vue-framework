define( [], function () {
'use strict';
return {
    el: '#index',
    data: function () {
        return {
            activeMenuId: null,
            openedMenus: [],
            menus: [ {
                id: 'm001',
                name: '메뉴-1'
            }, {
                id: 'm002',
                name: '메뉴-2'
            }, {
                id: 'm003',
                name: '메뉴-3'
            } ]
        };
    },
    methods: {
        openMenu: function ( menu ) {
            var isOpened = this.openedMenus.some( function ( m ) {
                return m.id === menu.id
            } );

            if ( isOpened ) {
                alert( menu.name + ' 메뉴는 이미 열려있습니다.' );
                return;
            }

            this.openedMenus.push( {
                id: menu.id,
                name: menu.name
            } );

            this.activeMenuId = menu.id;
        }
    },
    components: {
        'mdi-component': MdiComponent,
        'menu-list-component': MenuListComponent
    },
    template: `
        <div>
            <menu-list-component v-bind:menus="menus" v-bind:openMenu="openMenu"></menu-list-component>
            <mdi-component v-bind:openedMenus="openedMenus" v-bind:activeMenuId="activeMenuId"></mdi-component>
        </div>
    `
};

function MdiComponent ( resolve, reject ) {
    resolve( {
        props: [ 'openedMenus', 'activeMenuId' ],
        methods: {
            testAlert: function (msg){
                alert('컴포넌트 확인용'+msg)
            },
            changeTab: function ( menuId ) {
                var i = 0,
                    length = this.openedMenus.length;

                for ( ; i < length; i++ ) {
                    var menu = this.openedMenus[ i ];
                    if ( menuId === menu.id ) {
                        this.select( menu );
                        break;
                    }
                }
            },
            select: function ( menu ) {
                if ( !menu.isCompiled ) {
                    var vm = this,
                        id = menu.id;

                    /*
                     * 업무단 js 파일을 여기서 로드
                     * */
                    require( [
                        'vue',
                        '/assets/js/component/' + id + '/' + id + '.js',
                    ], function ( Vue, components ) {
                        components.el = '#menu' + id;
                        new Vue( components );
                    } );
                    menu.isCompiled = true;
                }
            }
        },
        template: `
            <div>
                <h1>MDI 영역</h1>
                <md-tabs class="md-primary" md-alignment="fixed" :md-active-tab="activeMenuId" @md-changed="changeTab">
                    <md-tab v-bind:id="menu.id" v-bind:md-label="menu.name" @click="select( menu )" v-for="menu in openedMenus">
                        <md-content class="md-default">
                            <div v-bind:id="'menu' + menu.id"></div>
                        </md-content>
                    </md-tab>
                </md-tabs>
            </div>
        `
    } );
}

function MenuListComponent ( resolve, reject ) {
    resolve( {
        props: [ 'openMenu', 'menus' ],
        methods: {
            select: function ( menu ) {
                this.openedMenus( menu );
            }
        },
        template: `
            <div>
                <h1>메뉴영역</h1>
                <md-list>
                    <md-list-item v-for="menu in menus">
                        <span class="md-list-item-text" @click="openMenu( menu )">{{ menu.name }} 클릭하세요.</span>
                    </md-list-item>
                </md-list>
            </div>
        `
    } );
}

} );
