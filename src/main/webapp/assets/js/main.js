( function ( window, requirejs, config ) {
'use strict';
requirejs.config( {
    baseUrl: 'assets/js',
    paths: {
        'text':       'lib/requirejs/text',
        'jquery':       'lib/jquery/1.12.3/jquery.min',
        'bootstrap':    'lib/bootstrap/3.4.1/bootstrap.min',
        'vue':          'lib/vue/2.5.13/vue.min',
        'vue-material': 'lib/vue-material/1.0.0-beta-10.2/vue-material.min',
        'index':        'index'
    },
    shim: {
        'vue-material': { deps: [ 'vue' ] },
        'index':        { deps: [ 'vue', 'vue-material' ] }
    }
} );

requirejs( [
    'vue',
    'vue-material',
    'index'
], function ( Vue, VueMaterial, index ) {
    Vue.use( VueMaterial.default );
    new Vue( index );
} );

} )( window, window.requirejs, window.config );
