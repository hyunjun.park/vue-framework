package ftptest;

import java.io.BufferedWriter;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.apache.commons.net.ftp.FTP;
import org.apache.commons.net.ftp.FTPClient;
import org.apache.commons.net.ftp.FTPFile;
import org.apache.commons.net.ftp.FTPReply;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class FtpTest {
    private static final Logger log = LoggerFactory.getLogger( "FTP" );
    private static final String username = "dichks";
    private static final String password = "9sghae!@#";

    private static List<String> files = new ArrayList<>();

    public static void main ( String[] args ) throws IOException {
        FTPClient ftp = new FTPClient();
        ftp.connect( "211.224.111.9", 21 );
        ftp.login( username, password );
        ftp.setControlEncoding( "utf-8" );
        ftp.setCharset( Charset.forName( "utf-8" ) );
        ftp.setFileType( FTP.BINARY_FILE_TYPE );

        showServerReply( ftp );

        listFolder( ftp, "/dichks" );

        int reply = ftp.getReplyCode();
        if ( !FTPReply.isPositiveCompletion( reply ) ) {
            ftp.disconnect();
            System.err.println( "FTP server refused connection..." );
            System.exit( 1 );
        }
        ftp.logout();

        log.debug( "write file-list" );

        Path filePath = Paths.get( "/Users", "Alchemist", "desktop" ).resolve( "nas-file-list.txt" );
        try ( BufferedWriter writer = Files.newBufferedWriter( filePath ); ) {
            String text = files.stream().collect( Collectors.joining( "\n" ) );
            writer.append( text );
            writer.flush();
        } catch ( Exception e ) {
            e.printStackTrace();
        }
    }

    private static void listFolder ( FTPClient ftp, String remotePath ) throws IOException {
        FTPFile[] remoteFiles = ftp.listFiles( new String( remotePath.getBytes(), "iso_8859_1" ) );
        for ( FTPFile remoteFile : remoteFiles ) {
            if ( !remoteFile.getName().equals( "." ) && !remoteFile.getName().equals( ".." ) ) {
                String remoteFilePath = remotePath + "/" + remoteFile.getName();
                if ( remoteFile.isDirectory() ) {
                    files.add( remoteFilePath );
                    listFolder( ftp, remoteFilePath );
                } else {
                    files.add( remoteFilePath );
                }
            }
        }
    }

    private static void showServerReply ( FTPClient ftp ) {
        String[] replies = ftp.getReplyStrings();
        if (replies != null && replies.length > 0) {
            for (String aReply : replies) {
                System.out.println("SERVER: " + aReply);
            }
        }
    }
}
